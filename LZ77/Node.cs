﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZ77
{
    public class Node
    {
        public string value;
        public int offset;
        public Node left;
        public Node right;
        public Node parent;

        public Node() { }
        public Node(string v)
        {
            value = v;
        }
        public Node(string v, int o)
        {
            value = v;
            offset = o;
        }
    }
}
