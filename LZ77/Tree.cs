﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZ77
{
  
    public class Tree
    {
        public Node start { get; set; }


        public  Tree()
        {
            start = null;

        }
        //public void Insert(Node newNode, Node _default)
        //{
        //    if (start == null)
        //    {
        //        start = newNode;
        //    }
        //    else
        //    {

        //        if (string.Compare(_default.value, newNode.value, StringComparison.InvariantCulture) < 0)
        //        {
        //            if (_default.right == null)
        //            {
        //                _default.right = newNode;
        //                newNode.parent = _default;
        //            }
        //            else
        //                this.Insert(newNode, _default.right);
        //        }
        //        else
        //        {

        //            if (_default.left == null)
        //            {
        //                _default.left = newNode;
        //                newNode.parent = _default;
        //            }
        //            else
        //                this.Insert(newNode, _default.left);
        //        }
        //    }

        //}

        public void Insert(Node newNode, Node _default)
        {
            if(start==null)
            {
                start = newNode;
            }
            else
            {
                          
                if(string.Compare(_default.value,newNode.value, StringComparison.InvariantCulture) < 0)
                {
                    if (_default.right == null)
                    {
                        _default.right = newNode;
                        newNode.parent = _default;
                    }
                    else
                        this.Insert(newNode, _default.right);
                }
                else
                {

                    if (_default.left == null)
                    {
                        _default.left = newNode;
                        newNode.parent = _default;
                    }
                    else
                        this.Insert(newNode, _default.left);
                }
            }
            
        }

        public void Delete(string value)
        {
            Node temp = this.start;

        }

        public Node Find(Node node,Node _default)
        {

            if (string.Compare(_default.value, node.value, StringComparison.InvariantCulture) < 0)
            {
                return Find(node, _default.right);
            }
            else if (string.Compare(_default.value, node.value, StringComparison.InvariantCulture) > 0)
            {
                return Find(node, _default.left);
            }
            else if (string.Compare(_default.value, node.value, StringComparison.InvariantCulture) == 0)
            {
                return _default;
            }
            else return null;
        }
        public void FindMatch(string value, List<Node> foundNodes,  Node next)
        {
            if (next != null)
            {
                if (String.Compare(next.value.Substring(0, value.Length), value) < 0)
                {
                    if (next.right != null)
                    {
                        FindMatch(value, foundNodes, next.right);
                    }
                }
                else if (String.Compare(next.value.Substring(0, value.Length), value) > 0)
                {
                    if (next.left != null)
                    {
                        FindMatch(value, foundNodes, next.left);
                    }
                }
                else
                {
                    foundNodes.Add(next);

                    if (next.left != null)
                    {
                        FindMatch(value, foundNodes, next.left);
                    }
                }
            }
        }

        public void PrintTree(Node _default)
        {
            
            if(_default!=null)
            {
                Console.WriteLine(_default.value + " " + _default.offset);
                PrintTree(_default.left);                
                PrintTree(_default.right);
            }            
            
        }
        //public List<Node> Find(Node node, Node _default)
        //{

        //    if (string.Compare(_default.value, node.value, StringComparison.InvariantCulture) < 0)
        //    {
        //        return Find(node, _default.right);
        //    }
        //    else if (string.Compare(_default.value, node.value, StringComparison.InvariantCulture) > 0)
        //    {
        //        return Find(node, _default.left);
        //    }
        //    else if (string.Compare(_default.value, node.value, StringComparison.InvariantCulture) == 0)
        //    {
        //        return _default;
        //    }
        //    else return null;
        //}
        public void Traverse(Node root)
        {
            if (root == null)
            {
                return;
            }

            Traverse(root.left);
            Traverse(root.right);
        }
    }


    
}
