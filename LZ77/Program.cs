﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZ77
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

   
    
    public class LZ77
    {
        //private const int RingBufferSize = 4096;
        //private const int UpperMatchLength = 18;
        //private const int LowerMatchLength = 2;
        //private const int None = RingBufferSize;
        //private static readonly int[] Parent = new int[RingBufferSize + 1];
        //private static readonly int[] LeftChild = new int[RingBufferSize + 1];
        //private static readonly int[] RightChild = new int[RingBufferSize + 257];
        //private static readonly ushort[] Buffer = new ushort[RingBufferSize + UpperMatchLength - 1];
        //private static int matchPosition, matchLength;
        //private static int matchPosition, matchLength;
        public const int searchLength = 16; 
        public const int lookAheadLength = 5;
        protected StreamReader mIn;
        protected StreamWriter mOut;
        public BalancedTree searchBuffer { get; set; } 
        public char[] lookAheadBuffer;
        public static int startLookAhead = 0; //pokazivac na pocetak reda look-ahead(buffer)
        public static int endLookAhead = 0; //pokazivac na kraj reda look-ahead(buffer)
        public LZ77()
        {
            lookAheadBuffer = new char[lookAheadLength];
            searchBuffer = new BalancedTree();

            //searchBuffer.height = (int)Math.Log(searchLength, 2);
        }
        public void Compress(string path)
        {
            mIn = new StreamReader(File.OpenRead(path), Encoding.UTF8); //reader za citanje iz fajla
            mOut = new StreamWriter(new FileStream(path + ".lz77", FileMode.OpenOrCreate)); //writer za upis kompresije u fajl             
            
            
            int Read=0, offset=0;
            

            char[] tempSearchBuff = new char[searchLength]; //privremeni buffer za inicijalizaciju stabla

            Read = mIn.Read(tempSearchBuff, 0, searchLength); 
            offset += Read +1;
            Read = mIn.Read(lookAheadBuffer, 0, lookAheadLength);

            endLookAhead = Read-1;
            
            
            //formiranje inicijalnog stabla pretrage(binary search tree)
            for (int i = 0; i <searchLength - lookAheadLength + 1; i++)
            {
                searchBuffer.Add(new string(tempSearchBuff).Substring(i, lookAheadLength), searchLength - i);
            }


            //stampanje cvorova
            Console.WriteLine("Nodes: ");
            searchBuffer.DisplayTree();

            string currentMatch;
            bool exist;
            int matchLength = 0;
            List<Node> foundNodes= new List<Node>();

          // do
          //{
                //pretraga stabla
                foundNodes.Clear();
                exist = false; // za proveru sto veceg match-a
                currentMatch = "";

                currentMatch += lookAheadBuffer[matchLength];

                while (!exist)
                {
                    exist = searchBuffer.FindMatch(currentMatch, foundNodes, exist);
                    if (exist)
                    {
                        //ako postoji match koji je duzi azurirati current match
                        startLookAhead= (startLookAhead+1)%lookAheadLength;
                        currentMatch += lookAheadBuffer[startLookAhead];

                    }

                    exist = !exist;
                }

                Console.WriteLine("Founds: ");

                for (int i = 0; i < foundNodes.Count; i++)
                {
                    //mOut.Write();
                    Console.WriteLine(foundNodes[i].value + ",  " + foundNodes[i].offset+ " " + currentMatch.Length);
                }
                Console.WriteLine("start pointer: "+startLookAhead);
                for (int i = 0; i < currentMatch.Length-1; i++)
                {
                    endLookAhead = (endLookAhead + 1) % lookAheadLength;
                    lookAheadBuffer[endLookAhead] = (char)mIn.Read();
                
                }
                Console.WriteLine();
                //stampanje look-ahead buffera
                startLookAhead = 0;
                endLookAhead = 4;
                int j = startLookAhead;
                do
                {
                    Console.Write(lookAheadBuffer[j]);
                    j = (j + 1) % lookAheadLength;
                } while (j != endLookAhead + 1);
            // } while (true);

            mIn.Close();
            mIn.Dispose();
            mOut.Flush();
            mOut.Close();


        }

        //        public  byte[] Compress(this byte[] ins, bool TestForCompressibility = false)
        //        {
        //            if (ins == null)
        //                throw new Exception("Input buffer is null.");
        //            if (ins.Length == 0)
        //                throw new Exception("Input buffer is empty.");
        //            //if (TestForCompressibility)
        //               // if ((int)Entropy(ins) > 61)
        //                  //  throw new Exception("Input buffer Cannot be compressed.");
        //            matchLength = 0;
        //            matchPosition = 0;
        //            CompressedSize = 0;
        //            DeCompressedSize = ins.Length;
        //            int length;
        //            var codeBuffer = new int[UpperMatchLength - 1];
        //            var outa = new GArray<byte>();
        //            var ina = new GArray<byte>(ins);
        //        return new byte[3];
        //        }



    }



    class Program
    {
        static void Main(string[] args)
        {
            LZ77 lz77 = new LZ77();


            lz77.Compress("file.txt");

            //BalancedTree tree = new BalancedTree();

            //tree.Add("brim");
            //tree.Add("come");
            //tree.Add("fast");
            //tree.Add("hers");
            //tree.Add("left");
            //tree.Add("from");
            //tree.Add("slow");
            //tree.Add("mine");
            //tree.Add("obey");
            //tree.Add("plug");
            //tree.Add("stay");
            //tree.Add("step");
            //tree.Add("went");

            //tree.DisplayTree();

            Console.ReadLine();
        }
    }
}
