﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZ77
{
    public class BalancedTree
    {
        Node root;
        //public int height { get; set; }
        public BalancedTree() { }
        public void Add(string data, int offset)
        {
            Node newItem = new Node(data, offset);
            if (root == null)
            {
                root = newItem;
            }
            else
            {
                root = RecursiveInsert(root, newItem);
            }
        }
        private Node RecursiveInsert(Node current, Node n)
        {
            if (current == null)
            {
                current = n;
                return current;
            }
            else if (String.Compare(n.value , current.value) < 0)
            {
                current.left = RecursiveInsert(current.left, n);
                current = balance_tree(current);
            }
            else if (String.Compare(n.value, current.value) > 0)
            {
                current.right = RecursiveInsert(current.right, n);
                current = balance_tree(current);
            }
            return current;
        }
        private Node balance_tree(Node current)
        {
            int b_factor = balance_factor(current);
            if (b_factor > 1)
            {
                if (balance_factor(current.left) > 0)
                {
                    current = RotateLL(current);
                }
                else
                {
                    current = RotateLR(current);
                }
            }
            else if (b_factor < -1)
            {
                if (balance_factor(current.right) > 0)
                {
                    current = RotateRL(current);
                }
                else
                {
                    current = RotateRR(current);
                }
            }
            return current;
        }

        private int balance_factor(Node current)
        {
            int l = getHeight(current.left);
            int r = getHeight(current.right);
            int b_factor = l - r;
            return b_factor;
        }

        private int getHeight(Node current)
        {
            int height = 0;
            if (current != null)
            {
                int l = getHeight(current.left);
                int r = getHeight(current.right);
                int m = max(l, r);
                height = m + 1;
            }
            return height;
        }
        private int max(int l, int r)
        {
            return l > r ? l : r;
        }
        public void DisplayTree()

        {
            if (root == null)
            {
                Console.WriteLine("Tree is empty");
                return;
            }
            InOrderDisplayTree(root);
            Console.WriteLine();
        }
        private void InOrderDisplayTree(Node current)
        {
            if (current != null)
            {
                InOrderDisplayTree(current.left);
                Console.Write("({0}-{1} ) ", current.value,current.offset);
                InOrderDisplayTree(current.right);
            }
        }

        public bool FindMatch(string value, List<Node> foundNodes, bool exist)
        {
            return FindMatch(value, foundNodes, root, exist);
        }
        public bool FindMatch(string value, List<Node> foundNodes, Node next, bool exist)
        {
            if (next != null)
            {
                if (String.Compare(next.value.Substring(0, value.Length), value) < 0)
                {
                    if (next.right != null)
                    {
                        exist= exist || FindMatch(value, foundNodes, next.right, exist);
                    }
                }
                else if (String.Compare(next.value.Substring(0, value.Length), value) > 0)
                {
                    if (next.left != null)
                    {
                        exist = exist || FindMatch(value, foundNodes, next.left, exist);
                    }
                }
                else
                {
                  
                    if (!exist)
                    {
                        foundNodes.Clear();
                    }
                    exist = true;

                    if (next.left != null)
                    {
                       exist = exist || FindMatch(value, foundNodes, next.left, exist);
                    }
                    foundNodes.Add(next);
                    if(next.right != null)
                    {
                        exist = exist || FindMatch(value, foundNodes, next.right, exist);
                    }
                }
            }

            return exist;
        }
        private Node RotateRR(Node parent)
        {
            Node pivot = parent.right;
            parent.right = pivot.left;
            pivot.left = parent;
            return pivot;
        }

        private Node RotateLL(Node parent)
        {
            Node pivot = parent.left;
            parent.left = pivot.right;
            pivot.right = parent;
            return pivot;
        }

        private Node RotateLR(Node parent)
        {
            Node pivot = parent.left;
            parent.left = RotateRR(pivot);
            return RotateLL(parent);
        }
        private Node RotateRL(Node parent)
        {
            Node pivot = parent.right;
            parent.right = RotateLL(pivot);
            return RotateRR(parent);
        }
    }
}
